# ENTITY REFERENCE TRANSLATION SYNC

When a reference field's value is modified (either added or deleted a reference),
this change will carry on to the parent entity's translations.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.

## MAINTAINERS

- ornagy - <https://www.drupal.org/u/ornagy>
- prompt-h - <https://www.drupal.org/u/prompt-h>
- BonkPrmpt - <https://www.drupal.org/u/bonkprmpt>
